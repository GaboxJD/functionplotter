package org.fjala.functionplotter;

public interface IFunction {
    float f(float x);
}
