package org.fjala.functionplotter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FunctionPlotter {
    private int imageWidth;
    private int imageHeight;
    private BufferedImage image;
    private Graphics2D g;

    public FunctionPlotter(int imageWidth, int imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    public void prepare() {
        this.g.setColor(Color.white);
        this.g.fillRect(0, 0, this.imageWidth, this.imageHeight);
        cuadricula();
        ejeDeCordenadas(this.imageWidth, this.imageHeight);
    }

    public void cuadricula() {
        for (int i = 0; i < 500; i = i + 50) {
            this.g.setColor(Color.black);
            this.g.drawLine(i, 0, i, 500);

            this.g.setColor(Color.black);
            this.g.drawLine(0, i, 500, i);
        } 
    }

    private void ejeDeCordenadas(int imageWidth, int imageHeight) {
        int wideMidPoint = imageWidth/2;
        int midPointHeight = imageHeight/2;
        
        this.g.setColor(Color.red);
        this.g.drawLine(0,wideMidPoint,imageWidth,midPointHeight);
        
        this.g.setColor(Color.red);
        this.g.drawLine(wideMidPoint,0,midPointHeight,imageHeight);
    }

    public void plot(IFunction function) {
        float domainFrom = -this.imageWidth/2;
        float domainTo = this.imageWidth/2;

        int lastU = 0;
        int lastV = 0;

        boolean firstPoint = true;
        boolean esIndeterminada = false;

        this.g.setColor(Color.blue);

        for (float x = domainFrom; x <= domainTo; x+=0.50) {
            float y = function.f(x);

            int u = (int)x + 250;
            int v = (int)y + 250;

            if (Float.isNaN(y)) {
                esIndeterminada = true;
            } else {
                if (!firstPoint) {
                    if (esIndeterminada) {
                        this.g.drawOval(lastU - 4, lastV - 4, 8, 8);
                        esIndeterminada = false;
                    }
                    this.g.drawLine(lastU, lastV, u, v);
                } else {
                    firstPoint = false;
                }

                lastU = u;
                lastV = v;
            }
        }
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }
}
