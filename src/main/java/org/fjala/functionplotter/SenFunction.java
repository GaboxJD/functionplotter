package org.fjala.functionplotter;

public class SenFunction implements IFunction {
    public float f(float x) {
        // f(x) = x

        float sen = (float)java.lang.Math.sin(x * Math.PI / 100);
        float resultadoSen = -(100 * sen);
        return resultadoSen;
    }
}