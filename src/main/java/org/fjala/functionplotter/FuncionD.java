package org.fjala.functionplotter;

public class FuncionD implements IFunction {
    public float f(float x) {
        float sen = (float)java.lang.Math.sin(x / 10);
        float f = (float) (2000 * sen / x);
        return -f;
    }
}