package org.fjala.functionplotter;

public class CuadraticFunction implements IFunction {
    public float f(float x) {
        return x * x;
    }
}
