package org.fjala.functionplotter;

public class RadicalFunction implements IFunction {
    public float f(float x) {

        float f = (float) (x * Math.sqrt(10) / (Math.sqrt(x + 10) - Math.sqrt(10)));
        return -f;
    }
}