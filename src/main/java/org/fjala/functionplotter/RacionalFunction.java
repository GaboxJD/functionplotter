package org.fjala.functionplotter;

public class RacionalFunction implements IFunction {
    public float f(float x) {

        float f = (float) (100 * Math.abs(x) / x);
        return -f;
    }
}