package org.fjala.functionplotter;

public class LinearFunction implements IFunction {
    public float f(float x) {
        // f(x) = x
        return x;
    }
}
